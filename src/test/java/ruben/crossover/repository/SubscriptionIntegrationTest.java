package ruben.crossover.repository;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ruben.crossover.JavaJournalApplication;
import ruben.crossover.domain.Journal;
import ruben.crossover.domain.Subscription;
import ruben.crossover.domain.User;
import ruben.crossover.util.TestLoginUtil;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import static org.hamcrest.Matchers.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.annotation.DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by rubenh on 4/4/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(JavaJournalApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
@DirtiesContext(classMode = BEFORE_EACH_TEST_METHOD)
@Profile("test")
public class SubscriptionIntegrationTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private SubscriptionRepository subscriptionRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private JournalRepository journalRepository;

    private Journal j1 = new Journal("1", "d1");
    private Journal j2 = new Journal("2", "d2");
    private User u1 = new User("1", "ln", "a", "d", "e", "p", "p", "ROLE_SUBSCRIBER");
    private Subscription s1 = new Subscription(u1, j1, new Date());

    @Value("${local.server.port}")
    int port;

    @Before
    public void setUp() {
        TestLoginUtil.login("ROLE_ADMIN");
        journalRepository.save(j1);
        journalRepository.save(j2);
        userRepository.save(u1);

        subscriptionRepository.save(s1);

        TestLoginUtil.logout();

        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void canGetSubscriptionRepo() {
        TestLoginUtil.login("ROLE_SUBSCRIBER");
        Subscription subscription = subscriptionRepository.findOne(s1.getId());
        Assert.assertEquals(subscription.getUser().getEmail(), u1.getEmail());
        Assert.assertEquals(subscription.getJournal().getName(), j1.getName());
        TestLoginUtil.logout();
    }

    /**
     * calls get all. should be 2 (data loader c test)
     *
     * @throws Exception
     */
    @Test
    public void canGetListSubscriptions() throws Exception {
        mockMvc.perform(get("/rest/subscriptions").with(user("admin").roles("ADMIN")))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$._embedded.subscriptions", hasSize(2)));
    }

    @Test
    public void canGetOneSubscription() throws Exception {
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        mockMvc.perform(get("/rest/subscriptions/" + s1.getId() + "?projection=complete").with(user("admin").roles("ADMIN")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.dateSubscribed", is(sdf.format(s1.getDateSubscribed()))))
                .andExpect(jsonPath("$.userEmail", is(u1.getEmail())))
                .andExpect(jsonPath("$.journalName", is(j1.getName())));
    }

    @Test
    public void canGetUserSubscriptions() throws Exception {
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        mockMvc.perform(get("/rest/subscriptions/search/findCurrentUser/").with(user(u1.getEmail()).roles("SUBSCRIBER")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded.subscriptions", hasSize(1)))
                .andDo(print())
                .andExpect(jsonPath("$._embedded.subscriptions[0].dateSubscribed", is(sdf.format(s1.getDateSubscribed()))))
                .andExpect(jsonPath("$._embedded.subscriptions[0].userEmail", is(u1.getEmail())))
                .andExpect(jsonPath("$._embedded.subscriptions[0].journalName", is(j1.getName())));
    }

}
