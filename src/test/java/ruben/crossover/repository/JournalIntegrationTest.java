package ruben.crossover.repository;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ruben.crossover.JavaJournalApplication;
import ruben.crossover.domain.Journal;

import java.util.Arrays;

import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by rubenh on 3/4/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(JavaJournalApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
@Profile("test")
public class JournalIntegrationTest {
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    JournalRepository journalRepository;

    private Journal j1 = new Journal("1", "d1");

    @Value("${local.server.port}")
    int port;

    @Before
    public void setUp() {

        journalRepository.save(Arrays.asList(
                j1,
                new Journal("2", "d2"),
                new Journal("3", "d3")));

        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

    }

    @Test
    @WithMockUser(roles={"ROLE_ADMIN"})
    public void canFetchJournal() throws Exception {
        Long j1Id = j1.getId();

        mockMvc.perform(get("/rest/journals/"+j1Id).with(user("admin").roles("ADMIN")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.description", is("d1")))
                .andExpect(jsonPath("$.name", is("1")));
    }

}
