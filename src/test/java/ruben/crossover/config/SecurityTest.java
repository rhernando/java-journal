package ruben.crossover.config;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ruben.crossover.JavaJournalApplication;
import ruben.crossover.repository.PublicationRepository;
import ruben.crossover.repository.SubscriptionRepository;

/**
 * Created by rhernando on 3/4/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(JavaJournalApplication.class)
@WebAppConfiguration
public class SecurityTest {

    @Autowired
    SubscriptionRepository subscriptionRepository;

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void getMessageUnauthenticated() {
        subscriptionRepository.findAll();
    }
}
