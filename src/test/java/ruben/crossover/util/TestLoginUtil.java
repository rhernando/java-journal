package ruben.crossover.util;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Created by rubenh on 6/4/16.
 */
public class TestLoginUtil {
    public static void login(String role) {
        SecurityContextHolder.getContext().setAuthentication(
                new UsernamePasswordAuthenticationToken("user", "user",
                        AuthorityUtils.createAuthorityList(role)));
    }

    public static void logout() {
        SecurityContextHolder.clearContext();
    }

}
