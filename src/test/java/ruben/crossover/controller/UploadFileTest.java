package ruben.crossover.controller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.multipart.MultipartFile;
import ruben.crossover.JavaJournalApplication;
import ruben.crossover.domain.Journal;
import ruben.crossover.domain.Publication;
import ruben.crossover.domain.User;
import ruben.crossover.repository.JournalRepository;
import ruben.crossover.repository.PublicationRepository;
import ruben.crossover.repository.UserRepository;
import ruben.crossover.service.FileStorage;
import ruben.crossover.util.TestLoginUtil;

import java.util.Date;

import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by rubenh on 6/4/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(JavaJournalApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
@Profile("test")
public class UploadFileTest {

    private PublicationRepository publicationRepository = Mockito.mock(PublicationRepository.class);
    private UserRepository userRepository = Mockito.mock(UserRepository.class);
    private JournalRepository journalRepository = Mockito.mock(JournalRepository.class);

    byte[] bytes = "data".getBytes();
    private FileUploadController controller;
    private MockMvc mockMvc;

    private FileStorage fileStorage = Mockito.mock(FileStorage.class);


    @Before
    public void setup() throws Exception {
        User u = new User();
        u.setEmail("email");
        Journal j = new Journal("N", "D");
        j.setUser(u);
        Publication p = new Publication("", "", new Date(), "", "");


        Mockito.when(fileStorage.store(any(MultipartFile.class))).thenReturn("1");

        Mockito.when(publicationRepository.save(any(Publication.class))).thenReturn(p);
        Mockito.when(userRepository.findByEmail(anyString())).thenReturn(u);
        Mockito.when(journalRepository.findOne(anyLong())).thenReturn(j);

        controller = new FileUploadController(journalRepository, publicationRepository, fileStorage, userRepository);
        this.mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        TestLoginUtil.login("ROLE_PUBLISHER");
    }

    @After
    public void clean() {
        TestLoginUtil.logout();
    }

    @Test
    public void canCreatePublication() throws Exception {
        String jsonPublication = "{\"title\":\"T\",\"author\":\"A\",\"pubDate\":\"1459893600000\",\"fileName\":\"file\",\"journal_id\":\"1\"}";
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/publish")
                .file("file", bytes)
                .param("publication", jsonPublication).
                        with(user("publisher@example.com").roles("PUBLISHER")))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.status", is("success")));

    }
}
