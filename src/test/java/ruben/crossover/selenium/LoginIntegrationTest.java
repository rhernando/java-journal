package ruben.crossover.selenium;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import static org.junit.Assert.assertEquals;

/**
 * Created by rhernando on 5/4/16.
 */
@Ignore
public class LoginIntegrationTest {
    private WebDriver browser;

    @Before
    public void setup() {
        //browser = new FirefoxDriver();
        System.setProperty("webdriver.chrome.driver", "/Users/rhernando/Downloads/chromedriver");
        browser  = new ChromeDriver();
    }

    @Test
    public void startTest() {
        browser.get("http://localhost:8080/");

        browser.findElement(By.id("login")).click();

        browser.findElement(By.id("username")).sendKeys("admin@example.com");
        browser.findElement(By.id("password")).sendKeys("Admin");

        browser.findElement(By.id("loginButton")).click();

        assertEquals("admin@example.com", browser.findElement(By.id("loginname")).getText());
    }

    @After
    public void tearDown() {
        browser.close();
    }
}
