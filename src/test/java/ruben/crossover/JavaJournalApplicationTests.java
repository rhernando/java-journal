package ruben.crossover;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.hateoas.config.EnableEntityLinks;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = JavaJournalApplication.class)
@WebAppConfiguration
public class JavaJournalApplicationTests {

	@Test
	public void contextLoads() {
	}

}
