package ruben.crossover.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by rhernando on 1/4/16.
 *
 * Controller to serve thymeleaf templates
 */
@Controller
public class Home {
    @RequestMapping(value = "/")
    public String index() {
        return "index";
    }

    @RequestMapping(value = "/login")
    public String login() {
        return "login";
    }
}
