package ruben.crossover.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ruben.crossover.domain.Journal;
import ruben.crossover.domain.Publication;
import ruben.crossover.domain.User;
import ruben.crossover.repository.JournalRepository;
import ruben.crossover.repository.PublicationRepository;
import ruben.crossover.repository.UserRepository;
import ruben.crossover.service.FileStorage;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

/**
 * Created by rhernando on 5/4/16.
 * <p>
 * Controller to manage PDF files and publications
 */
@RestController
public class FileUploadController {

    private PublicationRepository publicationRepository;
    private UserRepository userRepository;
    private JournalRepository journalRepository;
    private FileStorage fileStorage;


    @Autowired
    public FileUploadController(JournalRepository journalRepository,
                                PublicationRepository publicationRepository,
                                FileStorage fileStorage,
                                UserRepository userRepository) {
        this.publicationRepository = publicationRepository;
        this.fileStorage = fileStorage;
        this.userRepository = userRepository;
        this.journalRepository = journalRepository;
    }

    /**
     * Parses a publication in json to {@link Publication}
     * and calls the storage service {@link FileStorage} to store it
     *
     * @param publication publication in Json format
     * @param file        PDF file
     * @return json response
     */
    @PreAuthorize("hasRole('ROLE_PUBLISHER')")
    @RequestMapping(method = RequestMethod.POST, value = "/publish", produces = "application/json")
    @ResponseBody
    public String handleFileUpload(@RequestParam("publication") String publication,
                                   @RequestParam("file") MultipartFile file) {

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode retJson = mapper.createObjectNode();
        if (!file.isEmpty()) {
            try {
                Map<String, Object> pubJson = JsonParserFactory.getJsonParser().parseMap(publication);

                Date pubDate = new Date(Long.parseLong(pubJson.get("pubDate").toString()));
                Publication objPublication = new Publication(
                        pubJson.get("title").toString(),
                        pubJson.get("author").toString(),
                        pubDate,
                        pubJson.get("fileName").toString(),
                        fileStorage.store(file));

                String email = SecurityContextHolder.getContext().getAuthentication().getName();
                User user = this.userRepository.findByEmail(email);

                Journal j = this.journalRepository.findOne(Long.parseLong(pubJson.get("journal_id").toString()));
                if (!j.getUser().getEmail().equals(user.getEmail())) {
                    return retJson.put("status", "error").put("description", "Invalid journal").toString();
                }

                objPublication.setFromJournal(j);


                publicationRepository.save(objPublication);

                retJson.put("status", "success").put("publication", objPublication.getId()).
                        put("description", "Subscription created succesfully");

                return retJson.toString();
            } catch (Exception e) {
                e.printStackTrace();
                retJson.put("status", "error").put("description", e.getMessage());
            }
        } else {
            retJson.put("status", "error").put("description", "empty File");
        }

        return retJson.toString();
    }

    /**
     * @param fileId Stringified {@link org.bson.types.ObjectId}
     * @return PDF file
     * @throws IOException
     */
    @PreAuthorize("hasRole('ROLE_SUBSCRIBER')")
    @RequestMapping(value = "/download", method = RequestMethod.GET, produces = "application/pdf")
    public ResponseEntity<InputStreamResource> downloadPDFFile(@RequestParam("fileId") String fileId)
            throws IOException {


        return ResponseEntity
                .ok()
                .contentType(
                        MediaType.parseMediaType("application/octet-stream"))
                .body(new InputStreamResource(fileStorage.retrieve(fileId)));
    }
}
