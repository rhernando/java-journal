package ruben.crossover.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import ruben.crossover.domain.Journal;
import ruben.crossover.domain.Publication;

import java.util.List;

/**
 * Created by rhernando on 2/4/16.
 *
 * Restful repository for {@link Publication}
 */
public interface PublicationRepository extends CrudRepository<Publication, Long> {
    @RestResource(exported = false)
    @PreAuthorize("hasAnyRole('ROLE_PUBLISHER', 'ROLE_ADMIN')")
    @Override
    Publication save(Publication entity);

    @PreAuthorize("hasRole('ROLE_SUBSCRIBER')")
    @Query("select p from Publication p where p.fromJournal = ?")
    List<Publication> findByFromJournal(@Param("fromJournal") Journal fromJournal);

    @PreAuthorize("hasRole('ROLE_PUBLISHER')")
    @Query("select p from Publication p inner join p.fromJournal j inner join j.user u where u.email = ?#{principal.username}")
    List<Publication> findOwnedPublications();

    @PreAuthorize("hasRole('ROLE_PUBLISHER')")
    @Override
    void delete(Publication entity);
}
