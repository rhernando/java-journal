package ruben.crossover.repository;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import ruben.crossover.domain.Subscription;

import java.util.Date;

/**
 * Created by rubenh on 4/4/16.
 *
 * Projection to represent {@link Subscription}
 */
@Projection(name = "complete", types = Subscription.class)
public interface SubscriptionProjection {
    Date getDateSubscribed();

    @Value("#{target.getUser().getEmail()}")
    String getUserEmail();


    @Value("#{target.getJournal().getName()}")
    String getJournalName();

    @Value("#{target.getJournal().getId()}")
    String getJournalId();

}
