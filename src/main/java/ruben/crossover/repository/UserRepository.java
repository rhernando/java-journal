package ruben.crossover.repository;

import org.springframework.data.repository.Repository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import ruben.crossover.domain.Journal;
import ruben.crossover.domain.User;

/**
 * Created by rhernando on 1/4/16.
 * <p>
 * repository for {@link User} not available in the rest server
 */
@RepositoryRestResource(exported = false)
public interface UserRepository extends Repository<User, Long> {
    User save(User user);

    User findByEmail(String email);
}
