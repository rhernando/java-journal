package ruben.crossover.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import ruben.crossover.domain.Journal;
import ruben.crossover.domain.Subscription;

import java.util.List;

/**
 * Created by rhernando on 1/4/16.
 *
 * Restful repository for {@link Journal}
 */
public interface JournalRepository extends PagingAndSortingRepository<Journal, Long> {

    @PreAuthorize("hasRole('ROLE_PUBLISHER')")
    @Query("select j from Journal j inner join j.user u where u.email = ?#{principal.username}")
    List<Journal> findCurrentUser();

    @Override
    @PreAuthorize("hasAnyRole('ROLE_PUBLISHER', 'ROLE_ADMIN')")
    Journal save(Journal entity);
}

