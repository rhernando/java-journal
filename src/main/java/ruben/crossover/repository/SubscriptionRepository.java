package ruben.crossover.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import ruben.crossover.domain.Journal;
import ruben.crossover.domain.Subscription;
import ruben.crossover.domain.User;

import java.util.List;

/**
 * Created by rubenh on 4/4/16.
 * Restful repository for {@link Subscription}
 */
@PreAuthorize("hasAnyRole('ROLE_SUBSCRIBER', 'ROLE_ADMIN')")
@RepositoryRestResource(excerptProjection = SubscriptionProjection.class)
public interface SubscriptionRepository extends CrudRepository<Subscription, Long> {

    @Query("select s from  Subscription s inner join s.user u inner join s.journal j where u.email = ?#{principal.username}")
    List<Subscription> findCurrentUser();

}
