package ruben.crossover.repository;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import ruben.crossover.domain.Publication;
import ruben.crossover.domain.Subscription;

import java.util.Date;

/**
 * Created by rubenh on 4/4/16.
 *
 * Projection to represent {@link Publication}
 */
@Projection(name = "complete", types = Publication.class)
public interface PublicationProjection {
    Date getDateSubscribed();

    @Value("#{target.getFromJournal().getName()}")
    String getJournalName();

    @Value("#{target.getFromJournal().getId()}")
    String getJournalId();

    @Value("#{target.getFromJournal().getUser().getEmail()}")
    String getUserEmail();

}
