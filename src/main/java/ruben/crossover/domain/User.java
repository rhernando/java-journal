package ruben.crossover.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.ToString;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by rhernando on 1/4/16.
 * <p>
 * This class will represent a user in the application,
 * used for both publisher and subscriber.
 * We will differentiate the using roles, as maybe a user
 * can play both of them
 */
@Data
@ToString(exclude = "password")
@Entity
public class User {
    public static final PasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();

    private
    @Id
    @GeneratedValue
    Long id;
    private String firstName;
    private String lastName;
    private String address;
    private String description;

    @Column(unique = true)
    private String email;

    private String phone;

    private
    @JsonIgnore
    String password;

    @OneToMany(fetch = FetchType.EAGER)
    private List<Journal> ownsJournal;

    private String[] roles;

    public void setPassword(String password) {
        this.password = PASSWORD_ENCODER.encode(password);
    }

    public User() {
    }

    public User(String firstName, String lastName, String address, String description, String email, String phone, String password, String... roles) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.description = description;
        this.email = email;
        this.phone = phone;
        this.roles = roles;

        this.ownsJournal = new ArrayList<>();

        this.setPassword(password);
    }
}
