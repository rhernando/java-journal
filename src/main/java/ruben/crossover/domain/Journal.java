package ruben.crossover.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by rhernando on 1/4/16.
 * <p>
 * object to represent and store journals
 */
@Data
@Entity
public class Journal {
    private
    @Id
    @GeneratedValue
    Long id;

    private String name;
    private String description;

    @ManyToOne
    private User user;

    private Journal() {
    }

    public Journal(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
