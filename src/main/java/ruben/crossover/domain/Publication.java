package ruben.crossover.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by rhernando on 1/4/16.
 * <p>
 * object to represent and store publications from journals
 */
@Data
@Entity
public class Publication {
    private
    @Id
    @GeneratedValue
    Long id;

    private String title;
    private String author;

    @JsonFormat(pattern="yyyy-MM-dd")
    private Date pubDate;

    private String fileName;
    private String fileId;

    @ManyToOne
    private Journal fromJournal;

    private Publication() {
    }

    public Publication(String title, String author, Date pubDate, String fileName, String fileId) {
        this.title = title;
        this.author = author;
        this.pubDate = pubDate;
        this.fileName = fileName;
        this.fileId = fileId;
    }
}
