package ruben.crossover.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.util.Date;

/**
 * Created by rubenh on 1/4/16.
 * <p>
 * object to represent and store the subscriptions of the users to journals
 */
@Data
@Entity
public class Subscription {
    private @Id
    @GeneratedValue
    Long id;

    @OneToOne
    private User user;
    @OneToOne
    private Journal journal;

    private Date dateSubscribed;

    private Subscription(){}

    public Subscription(User user, Journal journal, Date dateSubscribed) {
        this.user = user;
        this.journal = journal;
        this.dateSubscribed = dateSubscribed;
    }
}
