package ruben.crossover.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by rhernando on 6/4/16.
 *
 * Interface to manage pdf files
 */
public interface FileStorage {
    String store(MultipartFile file) throws IOException;

    void remove(String id);


    InputStream retrieve(String id);
}
