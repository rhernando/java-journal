package ruben.crossover.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import ruben.crossover.domain.Journal;
import ruben.crossover.domain.User;
import ruben.crossover.repository.JournalRepository;
import ruben.crossover.repository.UserRepository;

/**
 * Created by rubenh on 1/4/16.
 * <p>
 * Assigns current user as the owner of a {@link Journal}
 */
@Component
@RepositoryEventHandler(Journal.class)
public class JournalEventHandler {

    private final UserRepository userRepository;


    @Autowired
    public JournalEventHandler(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @HandleBeforeCreate
    public void applyUserInformation(Journal journal) {

        String email = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = this.userRepository.findByEmail(email);

        journal.setUser(user);
    }
}
