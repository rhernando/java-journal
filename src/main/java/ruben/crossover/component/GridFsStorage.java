package ruben.crossover.component;

import com.mongodb.MongoClient;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import ruben.crossover.service.FileStorage;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by rhernando on 6/4/16.
 *
 * Implements {@link FileStorage} with a {@link GridFS} client from Mongo to store pdf files
 */
@Component
public class GridFsStorage implements FileStorage {
    @Value("${mongo.database}") String database;
    @Value("${mongo.collection}") String collection;

    @Override
    public String store(MultipartFile file) throws IOException {
        GridFS gridFS = new GridFS(new MongoClient().getDB(database), collection);
        GridFSInputFile gridFile = gridFS.createFile(file.getBytes());
        gridFile.save();

        return gridFile.getId().toString();
    }

    @Override
    public void remove(String id) {
        GridFS gridFS = new GridFS(new MongoClient().getDB(database), collection);

        try {
            ObjectId oid = new ObjectId(id);
            gridFS.remove(oid);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public InputStream retrieve(String id) {
        GridFS gridFS = new GridFS(new MongoClient().getDB(database), collection);
        ObjectId oid = new ObjectId(id);
        GridFSDBFile file = gridFS.findOne(oid);
        return file.getInputStream();

    }
}
