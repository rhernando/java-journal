package ruben.crossover.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import ruben.crossover.domain.Subscription;
import ruben.crossover.domain.User;
import ruben.crossover.repository.UserRepository;

/**
 * Created by rubenh on 1/4/16.
 * <p>
 * Assigns current user as the owner of a {@link Subscription}
 */
@Component
@RepositoryEventHandler(Subscription.class)
public class SubscriptionRestEventHandler {

    private final UserRepository userRepository;

    @Autowired
    public SubscriptionRestEventHandler(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @HandleBeforeCreate
    public void applyUserInformation(Subscription subscription) {

        String email = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = this.userRepository.findByEmail(email);

        subscription.setUser(user);
    }
}
