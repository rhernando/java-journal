package ruben.crossover.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import ruben.crossover.domain.Journal;
import ruben.crossover.domain.Publication;
import ruben.crossover.domain.Subscription;
import ruben.crossover.domain.User;
import ruben.crossover.repository.JournalRepository;
import ruben.crossover.repository.PublicationRepository;
import ruben.crossover.repository.SubscriptionRepository;
import ruben.crossover.repository.UserRepository;

import java.util.Date;

/**
 * Created by rhernando on 1/4/16.
 *
 * Preloaded data for testing purposes
 */
@Profile("test")
@Component
public class DataLoader implements CommandLineRunner {

    private final UserRepository userRepository;

    private final SubscriptionRepository subscriptionRepository;

    private final JournalRepository journalRepository;

    private final PublicationRepository publicationRepository;

    @Autowired
    public DataLoader(UserRepository userRepository, SubscriptionRepository subscriptionRepository, JournalRepository journalRepository, PublicationRepository publicationRepository) {
        this.subscriptionRepository = subscriptionRepository;
        this.userRepository = userRepository;
        this.journalRepository = journalRepository;
        this.publicationRepository = publicationRepository;
    }

    @Override
    public void run(String... strings) throws Exception {
        this.userRepository.save(new User("Main", "Admin", "example street, SP", "app admin", "admin@example.com",
                "+34959595959", "Admin", "ROLE_ADMIN"));

        SecurityContextHolder.getContext().setAuthentication(
                new UsernamePasswordAuthenticationToken("Admin", "admin",
                        AuthorityUtils.createAuthorityList("ROLE_ADMIN")));


        User user = new User("One", "Subscriber", "example street, SP", "subscriber", "subscriber@example.com",
                "+34959595959", "subscriber", "ROLE_SUBSCRIBER");
        this.userRepository.save(user);
        User publisher = new User("One", "Publisher", "example street, SP", "publisher", "publisher@example.com",
                "+34959595959", "publisher", "ROLE_PUBLISHER");
        this.userRepository.save(publisher);

        Journal journal = new Journal("Science", "first journal");
        journal.setUser(publisher);
        this.journalRepository.save(journal);
        this.journalRepository.save(new Journal("Bones", "second journal"));


        this.subscriptionRepository.save(new Subscription(user, journal, new Date()));

        Publication p = new Publication("TIT", "AUT", new Date(), "file","1");
        p.setFromJournal(journal);
        this.publicationRepository.save(p);

        SecurityContextHolder.clearContext();

    }
}
