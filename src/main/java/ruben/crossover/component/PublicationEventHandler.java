package ruben.crossover.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleBeforeDelete;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;
import ruben.crossover.domain.Publication;
import ruben.crossover.repository.UserRepository;
import ruben.crossover.service.FileStorage;

/**
 * Created by rubenh on 1/4/16.
 * <p>
 * Assigns current user as the owner of a {@link Publication}
 */
@Component
@RepositoryEventHandler(Publication.class)
public class PublicationEventHandler {
    private final UserRepository userRepository;

    private final FileStorage fileStorage;

    @Autowired
    public PublicationEventHandler(FileStorage fileStorage, UserRepository userRepository) {
        this.userRepository = userRepository;
        this.fileStorage = fileStorage;
    }

    @HandleBeforeDelete
    public void handleBeforeDelete(Publication publication) {
        this.fileStorage.remove(publication.getFileId());
    }

}
