package ruben.crossover.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import ruben.crossover.domain.Journal;
import ruben.crossover.domain.User;
import ruben.crossover.repository.JournalRepository;
import ruben.crossover.repository.UserRepository;

/**
 * Created by rhernando on 1/4/16.
 * <p>
 * Minimal data needed to make the Application run in demo mode
 */
@Profile("pdn")
@Component
public class DataLoaderProduction implements CommandLineRunner {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private final UserRepository userRepository;

    private final JournalRepository journalRepository;

    @Autowired
    public DataLoaderProduction(UserRepository userRepository, JournalRepository journalRepository) {
        this.userRepository = userRepository;
        this.journalRepository = journalRepository;
    }

    @Override
    public void run(String... strings) throws Exception {
        try {
            this.userRepository.save(new User("Main", "Admin", "example street, SP", "app admin", "admin@example.com",
                    "+34959595959", "Admin", "ROLE_ADMIN"));
        } catch (Exception e) {
            log.info("ADMIN ALREADY PRELOADED");
        }

        SecurityContextHolder.getContext().setAuthentication(
                new UsernamePasswordAuthenticationToken("Admin", "admin",
                        AuthorityUtils.createAuthorityList("ROLE_ADMIN")));

        User user = new User("John", "Subscriber", "example street, SP", "subscriber", "subscriber@example.com",
                "+34959595959", "subscriber", "ROLE_SUBSCRIBER");
        try {
            this.userRepository.save(user);
        } catch (Exception e) {
            log.info("SUBSCRIBER ALREADY PRELOADED");
        }

        User publisher = new User("Peter", "Publisher", "example street, SP", "publisher", "publisher@example.com",
                "+34959595959", "publisher", "ROLE_PUBLISHER");

        try {
            this.userRepository.save(publisher);
            Journal journal = new Journal("New England Journal of Medicine", "English");
            this.journalRepository.save(journal);
            publisher.getOwnsJournal().add(journal);
            this.userRepository.save(publisher);
        } catch (Exception e) {
            log.info("PUBLISHER ALREADY PRELOADED");
        }

        SecurityContextHolder.clearContext();

    }
}
