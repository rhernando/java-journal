package ruben.crossover.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ruben.crossover.repository.UserRepository;

/**
 * Created by rhernando on 2/4/16.
 * <p>
 * {@link UserDetailsService} implementations to get
 * correct user into the security context
 */
@Component
public class SpringDataJpaUserDetailsService implements UserDetailsService {
    private final UserRepository repository;

    @Autowired
    public SpringDataJpaUserDetailsService(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        ruben.crossover.domain.User user = this.repository.findByEmail(email);
        return new User(user.getEmail(), user.getPassword(),
                AuthorityUtils.createAuthorityList(user.getRoles()));
    }
}
