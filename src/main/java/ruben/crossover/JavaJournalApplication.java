package ruben.crossover;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.hateoas.config.EnableEntityLinks;

@SpringBootApplication
public class JavaJournalApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaJournalApplication.class, args);
	}
}
