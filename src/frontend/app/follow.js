/**
 *  Retrieve data from the REST server, using params
 * @param api   client api to call server
 * @param rootPath  base url to call the server
 * @param relArray  array of services
 * @returns {*|Promise}
 */
module.exports = function follow(api, rootPath, relArray) {

    var root = api({
        method: 'GET',
        path: rootPath
    });

    return relArray.reduce(function (root, arrayItem) {
        var rel = arrayItem.rel;
        return traverseNext(root, rel, arrayItem);
    }, root);

    function traverseNext(root, rel, arrayItem) {
        return root.then(function (response) {
            if (hasEmbeddedRel(response.entity, rel)) {
                return response.entity._embedded[rel];
            }
            if (!response.entity._links) {
                return api({
                    method: 'GET',
                    path: rootPath + "/" + rel,
                    params: arrayItem.params
                });
            }
            return api({
                method: 'GET',
                path: (response.entity._links[rel])? response.entity._links[rel].href : rootPath + "/" + rel,
                params: arrayItem.params
            });
        });
    }

    function hasEmbeddedRel(entity, rel) {
        return entity._embedded && entity._embedded.hasOwnProperty(rel);
    }

};