import React from 'react'
import {Clearfix, Col, Table, Button} from 'react-bootstrap'

/**
 * Renders the list of publications uploader by a user
 * 
 * Allows deletion
 */
export default class PublisherList extends React.Component {
    constructor(props) {
        super(props);
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleDelete(e) {
        this.props.deleteMyPub(e.target.id);
    }
    
    render() {
        if (this.props.publications) {
            let pubRows = this.props.publications.map(p=>
                <tr key={"view_"+p.fileId}>
                    <td>{p.title}</td>
                    <td>{p.author}</td>
                    <td>{p.pubDate}</td>
                    <td><Button bsStyle="danger" id={p._links.self.href}
                                onClick={this.handleDelete}>Delete</Button></td>
                </tr>
            );
            return (<div>
                <Col md={6} xs={10}>
                    <h3>My Publications</h3>
                    <Table condensed responsive>
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Author</th>
                            <th>Publication Date</th>
                            <th/>
                        </tr>
                        </thead>
                        <tbody>
                        {pubRows}
                        </tbody>
                    </Table>
                    <Clearfix/>

                </Col>

            </div>);
        } else {
            return (<div/>);
        }
    }
}


