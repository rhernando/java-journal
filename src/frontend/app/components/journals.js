import React from 'react'
import {Table, Row, Col, Input} from 'react-bootstrap'
import Journal from './journal'
import NewJournal from './new_journal'

/**
 * Renders a table with the list of journals in the system
 * 
 * Also provides pagination and navigation links
 * 
 * If the user is a publisher, will render a NewJournal component
 * 
 */
export  default class JournalList extends React.Component {
    constructor(props) {
        super(props);
        this.handleNavFirst = this.handleNavFirst.bind(this);
        this.handleNavPrev = this.handleNavPrev.bind(this);
        this.handleNavNext = this.handleNavNext.bind(this);
        this.handleNavLast = this.handleNavLast.bind(this);
        this.handleInput = this.handleInput.bind(this);
    }

    handleInput(e) {
        e.preventDefault();
        var pageSize = React.findDOMNode(this.refs.pageSize).value;
        if (/^[0-9]+$/.test(pageSize)) {
            this.props.updatePageSize(pageSize);
        } else {
            React.findDOMNode(this.refs.pageSize).value =
                pageSize.substring(0, pageSize.length - 1);
        }
    }

    handleNavFirst(e) {
        e.preventDefault();
        this.props.onNavigate(this.props.links.first.href);
    }

    handleNavPrev(e) {
        e.preventDefault();
        this.props.onNavigate(this.props.links.prev.href);
    }

    handleNavNext(e) {
        e.preventDefault();
        this.props.onNavigate(this.props.links.next.href);
    }

    handleNavLast(e) {
        e.preventDefault();
        this.props.onNavigate(this.props.links.last.href);
    }

    render() {
        var newJournal = (<div/>);
        if (this.props.publisher != undefined) {
            newJournal = <NewJournal onNewJournal={this.props.onNewJournal} />;
        }
        var journals = this.props.journals.map(journal =>
            <Journal key={journal._links.self.href} journal={journal} onSubscribe={this.props.onSubscribe}
                     viewPublications={this.props.viewPublications} subscriptions={this.props.subscriptions}/>
        );
        var navLinks = [];
        if ("first" in this.props.links) {
            navLinks.push(<button key="first" onClick={this.handleNavFirst}>&lt;&lt;</button>);
        }
        if ("prev" in this.props.links) {
            navLinks.push(<button key="prev" onClick={this.handleNavPrev}>&lt;</button>);
        }
        if ("next" in this.props.links) {
            navLinks.push(<button key="next" onClick={this.handleNavNext}>&gt;</button>);
        }
        if ("last" in this.props.links) {
            navLinks.push(<button key="last" onClick={this.handleNavLast}>&gt;&gt;</button>);
        }

        return (
            <div>
                <Col md={5} xs={10}>
                    <h3>Journal list</h3>
                    <Table condensed hover striped responsive>
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th/>
                        </tr>
                        </thead>
                        <tbody>
                        {journals}
                        </tbody>
                    </Table>
                    <Row>
                        <Col>View <input ref="pageSize" defaultValue={this.props.pageSize} onInput={this.handleInput}/>
                            items</Col>
                    </Row>
                    <Row>
                        <Col md={8} xs={10}>
                            {navLinks}
                        </Col>
                    </Row>
                    <Row>
                        {newJournal}
                    </Row>
                </Col>

            </div>
        )
    }
}


