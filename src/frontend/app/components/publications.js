import React from 'react'
import {Col, Table, Button} from 'react-bootstrap'

/**
 * Render the list of publications received
 * 
 * Allows the user to download the associated PDF file
 */
export default class Publications extends React.Component {
    constructor(props) {
        super(props);
        this.handlePubsPrev = this.handlePubsPrev.bind(this);
        this.handlePubsNext = this.handlePubsNext.bind(this);
        this.handlePubView = this.handlePubView.bind(this);
    }

    handlePubsPrev(e) {
        e.preventDefault();
        this.props.onNavigatePubs(this.props.links.prev.href);
    }

    handlePubsNext(e) {
        e.preventDefault();
        this.props.onNavigatePubs(this.props.links.next.href);
    }
    
    handlePubView(e){
        e.preventDefault();
        this.props.onPublicationView(e.target.id)
    }

    render() {
        if (this.props.publications) {
            let pubRows = this.props.publications.map(p=>
                <tr key={"view_"+p.fileId}>
                    <td>{p.title}</td>
                    <td>{p.author}</td>
                    <td>{p.pubDate}</td>
                    <td><Button bsStyle="info" id={p.fileId}
                                onClick={this.handlePubView}>View file</Button></td>
                </tr>
            );
            return (<div>
                <Col md={6} xs={10}>
                    <h3>Publication list</h3>
                    <Table condensed hover striped responsive>
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Author</th>
                            <th>Publication Date</th>
                            <th/>
                        </tr>
                        </thead>
                        <tbody>
                        {pubRows}
                        </tbody>
                    </Table>
                    <Col>View <input ref="pageSize" defaultValue={this.props.pubsSize}
                                     onInput={this.handlePubInput}/>
                        items</Col>
                </Col>

            </div>);
        } else {
            return (<div/>);
        }
    }
}