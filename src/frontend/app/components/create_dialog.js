import React from 'react'
import {Row, Col, Input, Button, FormControls, Clearfix} from 'react-bootstrap'
import DateTimeField from 'react-bootstrap-datetimepicker'

/**
 * Component to create a new publication and upload a PDF
 *
 * validates non-empty fields
 * select a journal among those owned by the publisher
 * return the new publication to parent
 * 
 */
export default class CreateDialog extends React.Component {

    constructor(props) {
        super(props);
        this.state = {title: 'warning', author: 'warning', pubDate: 'warning', pdf: 'warning', journal_id: 'warning'};
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    resetForm() {
        let fields = ['title', 'author', 'pdf', 'journal_id'];
        fields.forEach(f => {
            this.refs[f].getInputDOMNode().value = '';
        });
    }


    handleSubmit(e) {
        e.preventDefault();

        var newPublication = {};
        var error = false;
        var nState = {};

        if (this.refs.pdf.getInputDOMNode().files[0] === undefined) {
            error = true;
            nState['pdf'] = 'error';
        } else {
            nState['pdf'] = 'success';
        }
        let pdf = this.refs.pdf.getInputDOMNode().files[0];

        let fields = ['title', 'author', 'journal_id'];

        fields.forEach(f => {
            if (this.refs[f].getInputDOMNode().value.trim() != '') {
                newPublication[f] = this.refs[f].getInputDOMNode().value.trim();
                nState[f] = 'success';
            }
            else {
                error = true;
                nState[f] = 'error';
            }
        });

        if (this.refs.pubDate.getValue() == 'Invalid Date') {
            error = true;
            nState['pubDate'] = 'error';
        } else {
            newPublication.pubDate = this.refs.pubDate.getValue();
        }
        this.setState(nState);

        if (!error)
            this.props.onCreate(newPublication, pdf);
    }

    render() {
        if (this.props.publisher && this.props.publisher.length > 0) {
            let options = this.props.publisher.map(p => {
                return(<option value={p.id} key={p.id}>{p.name}</option>);
            });
            return (
                <Col md={6}>
                    <Row>
                        <h2>Upload new publication</h2>
                        <Col id="uploadPublication">

                            <Input bsStyle={this.state.title} type="text" ref="title" name="title" id="title"
                                   label="Title"
                                   placeholder="Title"/>
                            <Input bsStyle={this.state.author} type="text" name="author" id="author" label="Author"
                                   placeholder="Author" ref="author"/>
                            <Input bsStyle={this.state.pdf} type="file" id="pdf" name="pdf" label="Publication File"
                                   ref="pdf"
                                   accept=".pdf"/>
                            <Input type="select" label="Journal" placeholder="journal" ref="journal_id">
                                {options}
                            </Input>

                            <FormControls.Static value="Publication Date" className="strong bold"
                                                 labelClassName="col-xs-2"
                                                 wrapperClassName="col-xs-4"/>
                            <DateTimeField bsStyle={this.state.pubDate} id="pubDate" name="pubDate"
                                           inputFormat="MM/DD/YY"
                                           label="Publication Date" mode="date"
                                           ref="pubDate"/>

                        </Col>
                        <Clearfix/>
                        <Col md={8} xs={10}>
                            <Button block onClick={this.handleSubmit} value="Upload" id="sbmt" name="sbmt"
                                    type="submit">Upload</Button></Col>
                    </Row>
                </Col>
            )

        } else return <div/>;
    }

}

