import React from 'react'
import {Button} from 'react-bootstrap';

/**
 * Renders a journal
 * 
 * options are
 *  - see publications related (if subscribed)
 *  - subscrib to journal 
 *  
 */
export  default class Journal extends React.Component {
    constructor(props) {
        super(props);
        this.handleSubscribe = this.handleSubscribe.bind(this);
        this.handlePublications = this.handlePublications.bind(this);
        this.journalSubscription = this.journalSubscription.bind(this);

    }

    handleSubscribe() {
        this.props.onSubscribe(this.props.journal);
    }

    handlePublications() {
        this.props.viewPublications(this.props.journal);
    }

    journalSubscription(subscriptions) {
        if (subscriptions === undefined) {
            return false;
        }
        return subscriptions.find(s =>
            parseInt(s.journalId) == this.props.journal.id
        )
    }

    render() {
        let classname = this.journalSubscription(this.props.subscriptions) ? "success" : "info";

        let btnSubscribe =
            (this.props.subscriptions === undefined) ? <div/> :
                this.journalSubscription(this.props.subscriptions) ?
                    <Button bsStyle="warning" id={"view_"+this.props.journal.id}
                            onClick={this.handlePublications}>Publications</Button> :
                    <Button bsStyle="info" id={"subscribe_"+this.props.journal.id}
                            onClick={this.handleSubscribe}>Subscribe</Button>;

        return (
            <tr className={classname}>
                <td>{this.props.journal.name}</td>
                <td>{this.props.journal.description}</td>
                <td>
                    {btnSubscribe}
                </td>
            </tr>
        )
    }
}