import React from 'react'
import {Row, Col, Input, Button, FormControls, Clearfix} from 'react-bootstrap'

/**
 * Renders a form to create a new journal, which will be associated 
 * with the current user
 */
export  default class NewJournal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {name: 'warning', description: 'warning'};
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault();

        var newJournal = {};
        var error = false;
        var nState = {};

        let fields = ['name', 'description'];

        fields.forEach(f => {
            if (this.refs[f].getInputDOMNode().value.trim() != '') {
                newJournal[f] = this.refs[f].getInputDOMNode().value.trim();
                nState[f] = 'success';
            }
            else {
                error = true;
                nState[f] = 'error';
            }
        });

        this.setState(nState);

        if (!error)
            this.props.onNewJournal(newJournal);
        
    }

    render() {
        return (
            <Row>
                <h3>New journal</h3>
                <Col>
                    <Input bsStyle={this.state.name} type="text" ref="name" name="name" id="name"
                           label="Name"
                           placeholder="Name"/>
                    <Input bsStyle={this.state.description} type="text" name="desc" id="desc" label="Description"
                           placeholder="Description" ref="description"/>
                </Col>
                <Clearfix/>
                <Col md={6} xs={10}>
                    <Button block onClick={this.handleSubmit} value="Upload" id="sbmt" name="sbmt"
                            type="submit">Create</Button></Col>
            </Row>
        );
    }

}
