import React from 'react'
import ReactDOM from 'react-dom'
import {Grid, Alert, Row} from 'react-bootstrap'

const root = '/rest';

import client from './client'
const follow = require('./follow');

import superagent from 'superagent'


import CreateDialog from './components/create_dialog'
import JournalList from './components/journals'
import Publications from './components/publications'
import PublisherList from './components/publisher_list'

/**
 * Main component in the React Application
 * 
 * Will be the parent of the rest components, managing their state and data
 */
class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {pageSize: 20, pubsSize: 20, links: {}};
        this.updatePageSize = this.updatePageSize.bind(this);
        this.onCreate = this.onCreate.bind(this);
        this.onSubscribe = this.onSubscribe.bind(this);
        this.onNavigate = this.onNavigate.bind(this);
        this.onNavigatePubs = this.onNavigatePubs.bind(this);
        this.uploadMessage = this.uploadMessage.bind(this);
        this.viewPublications = this.viewPublications.bind(this);
        this.loadMyPublications = this.loadMyPublications.bind(this);
        this.deleteMyPub = this.deleteMyPub.bind(this);
        this.onNewJournal = this.onNewJournal.bind(this);
    }

    loadFromServer(pageSize) {
        follow(client, root, [
            {rel: 'journals', params: {size: pageSize}}]
        ).done(jc =>
            this.setState({
                journals: jc.entity._embedded.journals.map(j => j),
                pageSize: pageSize,
                links: jc.entity._links
            })
        );
    }

    loadUserInfo() {
        client({
            method: 'GET',
            path: root + "/subscriptions/search/findCurrentUser"
        }).catch(e => undefined).done(subscriptions =>
            this.setState({
                subscriptions: (subscriptions && subscriptions.entity._embedded) ? subscriptions.entity._embedded.subscriptions : undefined
            })
        );
        client({
            method: 'GET',
            path: root + "/journals/search/findCurrentUser"
        }).catch(e => undefined).done(journals =>
            (journals && journals.entity._embedded) ?
                this.loadMyPublications(journals.entity._embedded.journals) : undefined
        )
    }

    loadMyPublications(journal) {
        client({
            method: 'GET',
            path: root + "/publications/search/findOwnedPublications"
        }).catch(e => undefined).done(publications =>
            this.setState({
                publisher: journal || this.state.publisher,
                myPublications: (publications && publications.entity._embedded) ? publications.entity._embedded.publications : undefined
            })
        );
    }

    uploadMessage(response) {
        if (response.body.status === 'success') {
            this.loadMyPublications();
            this.refs.dialog.resetForm();
        }
        this.setState({
            alert: {kind: response.body.status, message: response.body.description}
        });

    }

    onNewJournal(newJournal) {
        client({
            method: 'POST',
            path: root + "/journals",
            entity: newJournal,
            headers: {'Content-Type': 'application/json'}
        }).catch(r => undefined).done(r => {
                this.loadUserInfo();
                this.loadFromServer(this.state.pageSize);
            }
        );
    }

    onCreate(newPublication, pdf) {

        newPublication.fileName = pdf.name;
        var form = new FormData();
        form.append('publication', JSON.stringify(newPublication));
        form.append('file', pdf);

        superagent.post('/publish')
            .send(form)
            .end((err, response) =>
                this.uploadMessage(response)
            );
    }

    onSubscribe(journal) {
        let subscription = {journal: journal._links.self.href, dateSubscribed: new Date()};
        client({
            method: 'POST',
            path: root + "/subscriptions",
            entity: subscription,
            headers: {'Content-Type': 'application/json'}
        }).done(r =>
            this.loadUserInfo()
        );
    }

    onNavigate(navUri) {
        client({method: 'GET', path: navUri}).done(journalCollection => {
            this.setState({
                journals: journalCollection.entity._embedded.journals,
                pageSize: this.state.pageSize,
                links: journalCollection.entity._links
            });
        });
    }

    updatePageSize(pageSize) {
        if (pageSize !== this.state.pageSize) {
            this.loadFromServer(pageSize);
        }
    }

    componentDidMount() {
        this.loadFromServer(this.state.pageSize);
        this.loadUserInfo();
    }

    onNavigatePubs(navUri) {
        client({method: 'GET', path: navUri}).done(publicationCollection => {
            this.setState({
                publications: publicationCollection.entity._embedded.publications,
                pageSize: this.state.pubsSize,
                pubLinks: publicationCollection.entity._links
            });
        });
    }

    deleteMyPub(link) {
        client({
            method: 'DELETE',
            path: link,
            headers: {'Content-Type': 'application/json'}
        }).done(r =>
            this.loadMyPublications()
        );
    }

    loadPublications(pageSize, journal) {
        follow(client, root, [
            {
                rel: 'publications/search/findByFromJournal',
                params: {pubsSize: pageSize, fromJournal: journal._links.self.href}
            }]
        ).done(pc =>
            this.setState({
                publications: pc.entity._embedded.publications.map(j => j),
                pubsSize: pageSize,
                pubLinks: pc.entity._links
            })
        );
    }

    onPublicationView(fileId) {
        window.open("/download?fileId=" + fileId);
    }

    viewPublications(journal) {
        this.loadPublications(this.state.pubsSize, journal);
    }

    render() {
        let alertMsg = (this.state.alert) ?
            <Alert bsStyle={this.state.alert.kind}>
                {this.state.alert.message}
            </Alert>
            : <div/>

        return (
            <Grid id="wrapper" fluid className="container">
                {alertMsg}
                <Row>
                    <CreateDialog ref="dialog" onCreate={this.onCreate} publisher={this.state.publisher}/>
                    <PublisherList publisher={this.state.publisher}
                                   publications={this.state.myPublications}
                                   deleteMyPub={this.deleteMyPub}/>
                </Row>
                <JournalList journals={this.state.journals || []}
                             links={this.state.links}
                             pageSize={this.state.pageSize}
                             subscriptions={this.state.subscriptions}
                             onNavigate={this.onNavigate}
                             onSubscribe={this.onSubscribe}
                             updatePageSize={this.updatePageSize}
                             viewPublications={this.viewPublications}
                             publisher={this.state.publisher}
                             onNewJournal={this.onNewJournal}/>

                <Publications onNavigatePubs={this.onNavigatePubs}
                              publications={this.state.publications}
                              pubsSize={this.state.pubsSize}
                              onPublicationView={this.onPublicationView}/>
            </Grid>
        )
    }
}

ReactDOM.render(
    <App />,
    document.getElementById('react')
);