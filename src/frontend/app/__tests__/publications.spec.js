'use strict';

jest.unmock('../components/publications');

import React from 'react';
import TestUtils from 'react-addons-test-utils';
import Publications from '../components/publications';

describe('Publications', () => {
    it('render empty publications table', () => {
        let f = jest.genMockFunction();
        let list = TestUtils.renderIntoDocument(
            <Publications onNavigatePubs={f} />
        );
        var rows = TestUtils.scryRenderedDOMComponentsWithTag(list, 'tr');
        expect(rows.length).toEqual(0);

    });

    it('render table with publications ', () => {
        let f = jest.genMockFunction();
        let list = TestUtils.renderIntoDocument(
            <Publications onNavigatePubs={f} publications={[{}]} />
        );
        var rows = TestUtils.scryRenderedDOMComponentsWithTag(list, 'tr');
        expect(rows.length).toEqual(2);

    });

    it('download pdf from publications ', () => {
        let f = jest.genMockFunction();
        let fView = jest.genMockFunction();
        let list = TestUtils.renderIntoDocument(
            <Publications onNavigatePubs={f} onPublicationView={fView} publications={[{fileId:"22"}]} />
        );
        let elems = TestUtils.scryRenderedDOMComponentsWithTag(list, 'button');
        TestUtils.Simulate.click(elems[0]);
        expect(fView).toBeCalled();

    });

});
