'use strict';

jest.unmock('../components/journals');
jest.unmock('../components/journal');

import React from 'react';
import TestUtils from 'react-addons-test-utils';
import JournalList from '../components/journals';

describe('JournalList', () => {

    it('render empty journals table', () => {
        let list = TestUtils.renderIntoDocument(
            <JournalList journals={[]}
                         links={{}}
                         subscriptions={[]}
                         pageSize={1}
                         onNavigate={function(){console.log("a")}}
                         onSubscribe={function(){console.log("b")} }
                         updatePageSize={function(){console.log("c")} }/>
        );
        var rows = TestUtils.scryRenderedDOMComponentsWithTag(list, 'tr');
        expect(rows.length).toEqual(1);

    });

    it('render 1 element journals table', () => {
        let list = TestUtils.renderIntoDocument(
            <JournalList journals={[{_links: {self:{href:'r'}},name:"A"} ]}
                         links={{}}
                         subscriptions={[]}
                         pageSize={{}}
                         onNavigate={function(){console.log("a")}}
                         onSubscribe={function(){console.log("b")} }
                         updatePageSize={function(){console.log("c")} }/>
        );

        var rows = TestUtils.scryRenderedDOMComponentsWithTag(list, 'tr');
        expect(rows.length).toEqual(2);

    });

});
