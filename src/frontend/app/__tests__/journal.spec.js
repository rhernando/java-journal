'use strict';

jest.unmock('../components/journal');

import React from 'react';
import TestUtils from 'react-addons-test-utils';
import Journal from '../components/journal';

var WrapperTable = React.createClass({
    render: function () {
        return (
            <table>
                <tbody>{this.props.children}</tbody>
            </table>
        );
    }
});

describe('Journal', () => {

    it('renders a journal', () => {
        let list = TestUtils.renderIntoDocument(
            <WrapperTable><Journal key={"a"} journal={{id:"1", name:"a"}} subscriptions={[]}
                                   onSubscribe={function(){console.log(1)}}/></WrapperTable>
        );
        var rows = TestUtils.scryRenderedDOMComponentsWithTag(list, 'tr');
        expect(rows.length).toEqual(1);

    });

    it('renders a subscribed journal', () => {
        let list = TestUtils.renderIntoDocument(
            <WrapperTable><Journal key={"a"} journal={{id:"1", name:"a"}} subscriptions={[{journalId:"1"}]}
                                   onSubscribe={function(){console.log(1)}}/></WrapperTable>
        );
        let rows = TestUtils.scryRenderedDOMComponentsWithTag(list, 'tr');
        expect(rows.length).toEqual(1);
        let success = TestUtils.findRenderedDOMComponentWithClass(list, 'success');
        expect(rows.length).toBeDefined();

    });

    it('can subscribe an unsuscribed journal', () => {

        let f = jest.genMockFunction();

        let list = TestUtils.renderIntoDocument(
            <WrapperTable>
                <Journal key={"a"} journal={{id:"1", name:"a"}} subscriptions={[]} onSubscribe={f}
                         viewPublications={f}/>
            </WrapperTable>
        );

        let elems = TestUtils.scryRenderedDOMComponentsWithTag(list, 'button');
        expect(elems.length).toEqual(1);
        expect(elems[0].className).toEqual("btn btn-info");

        TestUtils.Simulate.click(elems[0]);
        expect(f).toBeCalled();

    });

    it('can not subscribe a suscribed journal', () => {

        let f = jest.genMockFunction();

        let list = TestUtils.renderIntoDocument(
            <WrapperTable>
                <Journal key={"a"} journal={{id:"1", name:"a"}} subscriptions={[{journalId:"1"}]} onSubscribe={f}
                         viewPublications={f}/>
            </WrapperTable>
        );

        let elems = TestUtils.scryRenderedDOMComponentsWithTag(list, 'button');
        expect(elems.length).toEqual(1);
        expect(elems[0].className).toEqual("btn btn-warning");

    });

    it('can view a suscribed journal', () => {

        let fSusc = jest.genMockFunction();
        let fView = jest.genMockFunction();

        let list = TestUtils.renderIntoDocument(
            <WrapperTable>
                <Journal key={"a"} journal={{id:"1", name:"a"}} subscriptions={[{journalId:"1"}]} onSubscribe={fSusc}
                         viewPublications={fView}/>
            </WrapperTable>
        );

        let elems = TestUtils.scryRenderedDOMComponentsWithTag(list, 'button');
        expect(elems.length).toEqual(1);
        TestUtils.Simulate.click(elems[0]);
        expect(fSusc).not.toBeCalled();
        expect(fView).toBeCalled();

    });

});
