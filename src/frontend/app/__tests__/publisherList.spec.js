'use strict';

jest.unmock('../components/publisher_list');

import React from 'react';
import TestUtils from 'react-addons-test-utils';
import PublisherList from '../components/publisher_list';

describe('PublisherList', () => {
    it('render empty publications table', () => {
        let f = jest.genMockFunction();
        let list = TestUtils.renderIntoDocument(
            <PublisherList onNavigatePubs={f}/>
        );
        var rows = TestUtils.scryRenderedDOMComponentsWithTag(list, 'tr');
        expect(rows.length).toEqual(0);

    });

    it('render table with publications ', () => {
        // Render a checkbox with label in the document
        let f = jest.genMockFunction();
        let list = TestUtils.renderIntoDocument(
            <PublisherList onNavigatePubs={f} publications={[{_links : {self: {href : "l"}}}]}/>
        );
        var rows = TestUtils.scryRenderedDOMComponentsWithTag(list, 'tr');
        expect(rows.length).toEqual(2);

    });

    it('render table with publications btn', () => {
        // Render a checkbox with label in the document
        let fDel = jest.genMockFunction();
        let fView = jest.genMockFunction();
        let list = TestUtils.renderIntoDocument(
            <PublisherList onNavigatePubs={fView} deleteMyPub={fDel} publications={[{_links : {self: {href : "l"}}}]}/>
        );
        let elems = TestUtils.scryRenderedDOMComponentsWithTag(list, 'button');
        TestUtils.Simulate.click(elems[0]);
        expect(fDel).toBeCalled();

    });

});
