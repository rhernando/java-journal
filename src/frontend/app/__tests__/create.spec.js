'use strict';

jest.unmock('../components/create_dialog');

import React from 'react';
import TestUtils from 'react-addons-test-utils';
import CreateDialog from '../components/create_dialog';

describe('CreateDialog', () => {

    it('renders empty dialog', () => {
        let f = jest.genMockFunction();

        let d = TestUtils.renderIntoDocument(
            <CreateDialog attributes={{}} onCreate={f} publisher={{}} />
        );
        var rows = TestUtils.scryRenderedDOMComponentsWithTag(d, 'form');
        expect(rows.length).toEqual(0);

    });

    it('renders form dialog', () => {
        let f = jest.genMockFunction();

        let d = TestUtils.renderIntoDocument(
            <CreateDialog attributes={[{a:1}]} onCreate={f} publisher={[{id:1}]} />
        );
        var rows = TestUtils.scryRenderedDOMComponentsWithTag(d, 'input');
        expect(rows.length).toBeGreaterThan(1);

    });

    it('not submit empty form', () => {
        let f = jest.genMockFunction();

        let d = TestUtils.renderIntoDocument(
            <CreateDialog attributes={[{a:1}]} onCreate={f} publisher={{id:1}} />
        );


        var btn = TestUtils.scryRenderedComponentsWithType(d, 'submit');
        TestUtils.Simulate.click(btn);
        expect(f).not.toBeCalled();

    });



});
