var path = require('path');

module.exports = {
    entry: './app/app.js',
    devtool: 'sourcemaps',
    cache: true,
    debug: true,
    output: {
        path: path.resolve(__dirname, '../../target/classes/static/built'), 
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {
                test: /src\/.+.js$/,
                exclude: /node_modules/,
                loader: 'babel'
            }
        ]
    }
};
