#!/bin/bash

echo -------------- JAVA JOURNAL DEPLOYMENT -------------
mvn clean install -Dspring.profiles.active=test

cd target

echo -------------- COMPILATION FINISHED -------------
echo
echo -------------- RUNNING JAR  -------------

java -Dspring.profiles.active=pdn -jar *jar